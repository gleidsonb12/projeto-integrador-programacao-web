<?php
require_once "conn.php";
?>

<!DOCTYPE html>
<html lang="pt_BR">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Programação Web</title>

  <!-- folha de estilo -->
  <link rel="stylesheet" href="style.css" />
</head>

<body>

  <h1>Cadastrar novo Livro</h1>

  <div id="bloco">
    <form action="index.php" method="POST">
      <input type="text" id="titulo" name="titulo" placeholder="Título do Livro" required>
      <input type="text" id="autor" name="autor" placeholder="Autor do Livro" required>
      <select id="editora" name="editora" required>
        <option value="" selected disabled>Selecione a Editora</option>

        <?php // lista os registros do BD //
        $sql = "SELECT * FROM editora";
        $resul = mysqli_query($conexao, $sql);

        while ($row = $resul->fetch_object()) { ?>
          <option value="<?php echo $row->id ?>">
            <?php echo $row->nome ?>
          </option>
        <?php } ?>

      </select>

      <input type="number" id="ano" name="ano" placeholder="Ano de Publicação do Livro" required>
      <input type="text" id="preco" name="preco" placeholder="Preço do Livro" required>
      <input type="number" id="quantidade" name="quantidade" placeholder="Qtd. de Exemplares" required>

      <select id="tipo" name="tipo" required>
        <option value="" selected disabled>Selecione a Classificação</option>
        <option value="1">Romance</option>
        <option value="2">Aventura</option>
        <option value="3">Ficção</option>
        <option value="4">Auto-Ajuda</option>
        <option value="5">Mistério</option>
        <option value="6">Ação</option>
        <option value="7">Religioso</option>
        <option value="8">Didático</option>
        <option value="9">Suspense</option>
      </select>

      <input type="submit" value="Cadastrar">
    </form>
  </div>

  <hr />

  <a href="index.php" id="home-btn">HOME</a>

</body>

</html>