<?php
require_once "conn.php";

// se postado, faz o INSERT no Banco //
if (isset($_POST['editora']) && !isset($_POST['edit'])) {
  $titulo = $_POST['titulo'];
  $autor = $_POST['autor'];
  $editora = $_POST['editora'];
  $preco = $_POST['preco'];
  $quantidade = $_POST['quantidade'];
  $ano = $_POST['ano'];
  $tipo = $_POST['tipo'];

  $sql_insert = (mysqli_query($conexao, "INSERT INTO acervo(idEditora, titulo, autor, ano, preco, quantidade, tipo) VALUES ('$editora', '$titulo', '$autor','$ano','$preco', '$quantidade', '$tipo')"));

  echo "<script>alert('Cadastrado com Sucesso!');</script>";
}

// se postado, faz o UPDATE no Banco //
if (isset($_POST['edit'])) {
  $id = $_POST['edit'];
  $titulo = $_POST['titulo'];
  $autor = $_POST['autor'];
  $editora = $_POST['editora'];
  $preco = $_POST['preco'];
  $quantidade = $_POST['quantidade'];
  $ano = $_POST['ano'];
  $tipo = $_POST['tipo'];

  $sql_update = (mysqli_query($conexao, "UPDATE acervo SET idEditora='$editora', titulo='$titulo', autor='$autor', ano='$ano', preco='$preco', quantidade='$quantidade', tipo='$tipo' WHERE id='$id'"));

  echo "<script>alert('Atualizado com Sucesso!');</script>";
}

// se existir, pega o get del //
if (isset($_GET['del'])) {
  $sql_logis = (mysqli_query($conexao, "DELETE FROM `acervo` WHERE id = '$_GET[del]'"));

  echo "<script>alert('Registro apagado com sucesso!');</script>";
}
?>

<!DOCTYPE html>
<html lang="pt_BR">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Programação Web</title>

  <!-- folha de estilo -->
  <link rel="stylesheet" href="style.css" />
</head>

<body>

  <form action="index.php" method="GET">
    <input type="hidden" name="pesquisar">
    <input type="text" placeholder="Pesquisar por título" name="input_pesquisar" class="input_pesquisar" />
    <input type="submit" value="Pesquisar" class="pesquisar-btn" />
  </form>

  <h1>Acervo de Livros</h1>

  <table id="customers">
    <tr>
      <th>Id</th>
      <th>Editora</th>
      <th>Titulo</th>
      <th>Autor</th>
      <th>Ano</th>
      <th>Preço</th>
      <th>Qtd.</th>
      <th>Tipo</th>
      <th colspan="2">Ações</th>
    </tr>

    <?php // lista os registros do BD //
    if (isset($_GET['pesquisar'])) {
      $busca = $_GET['input_pesquisar'];
      $sql = "SELECT * FROM acervo WHERE titulo LIKE '%$busca%'";
    } else {
      $sql = "SELECT * FROM acervo";
    }
    
    $resul = mysqli_query($conexao, $sql);

    while ($row = $resul->fetch_object()) { ?>

      <tr>
        <td><?php echo $row->id ?></td>
        <td>
          <?php
          $sqlEd = "SELECT * FROM editora WHERE id = $row->idEditora LIMIT 1";
          $editora = mysqli_query($conexao, $sqlEd);
          echo $editora->fetch_object()->nome;
          ?>
        </td>
        <td><?php echo $row->titulo ?></td>
        <td><?php echo $row->autor ?></td>
        <td><?php echo $row->ano ?></td>
        <td><?php echo "R$ " . $row->preco ?></td>
        <td><?php echo $row->quantidade ?></td>

        <?php
        switch ($row->tipo) {
          case 1:
            $tipo = "Romance";
            break;
          case 2:
            $tipo = "Aventura";
            break;
          case 3:
            $tipo = "Ficção";
            break;
          case 4:
            $tipo = "Auto-Ajuda";
            break;
          case 5:
            $tipo = "Mistério";
            break;
          case 6:
            $tipo = "Ação";
            break;
          case 7:
            $tipo = "Religioso";
            break;
          case 8:
            $tipo = "Didático";
            break;
          case 9:
            $tipo = "Suspense";
            break;
        } ?>
        <td><?php echo $tipo ?></td>
        <td>
          <a href="edit.php?<?php echo "edit=" . $row->id ?>" id="edit-btn" name="edit-btn">Editar</a>
        </td>
        <td>
          <a href="index.php?<?php echo "del=" . $row->id ?>" id="del-btn" onclick="return confirm('Tem certeza que deseja apagar o registro?')">Deletar</a>
        </td>
      </tr>

    <?php } ?>


  </table>

  <a href="cad.php" id="add-btn">Cadastrar</a>

</body>

</html>