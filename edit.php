<?php
require_once "conn.php";
?>

<!DOCTYPE html>
<html lang="pt_BR">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Programação Web</title>

  <!-- folha de estilo -->
  <link rel="stylesheet" href="style.css" />
</head>

<body>

  <h1>Editar Cadastro de Livro</h1>

  <?php // lista os registros do BD //
  $sql = "SELECT * FROM acervo WHERE id = '$_GET[edit]'";
  $resul = mysqli_query($conexao, $sql);

  while ($row = $resul->fetch_object()) { ?>

    <div id="bloco">
      <form action="index.php" method="POST">
        <!-- envia de form oculta o id do registro que será editado-->
        <input type="hidden" name="edit" value="<?php echo $row->id ?>">

        <input type="text" id="titulo" name="titulo" value="<?php echo $row->titulo ?>" required>
        <input type="text" id="autor" name="autor" placeholder="Autor do Livro" value="<?php echo $row->autor ?>" required>
        <select id="editora" name="editora" required>
          <option value="" selected disabled>Selecione a Editora</option>

          <?php // lista os registros do BD //
          $sql_ed = "SELECT * FROM editora";
          $resul_ed = mysqli_query($conexao, $sql_ed);

          while ($row_ed = $resul_ed->fetch_object()) { ?>
            <option value="<?php echo $row_ed->id ?>" <?php echo $row_ed->id == 1 ? "selected" : NULL ?>>
              <?php echo $row_ed->nome ?>
            </option>
          <?php } ?>

        </select>

        <input type="number" id="ano" name="ano" placeholder="Ano de Publicação do Livro" value="<?php echo $row->ano ?>" required>
        <input type="text" id="preco" name="preco" placeholder="Preço do Livro" value="<?php echo $row->preco ?>" required>
        <input type="number" id="quantidade" name="quantidade" placeholder="Qtd. de Exemplares" value="<?php echo $row->quantidade ?>" required>

        <select id="tipo" name="tipo" required>
          <option value="" selected disabled>Selecione a Classificação</option>
          <option value="1" <?php echo $row->id == 1 ? "selected" : NULL ?>>Romance</option>
          <option value="2" <?php echo $row->id == 2 ? "selected" : NULL ?>>Aventura</option>
          <option value="3" <?php echo $row->id == 3 ? "selected" : NULL ?>>Ficção</option>
          <option value="4" <?php echo $row->id == 4 ? "selected" : NULL ?>>Auto-Ajuda</option>
          <option value="5" <?php echo $row->id == 5 ? "selected" : NULL ?>>Mistério</option>
          <option value="6" <?php echo $row->id == 6 ? "selected" : NULL ?>>Ação</option>
          <option value="7" <?php echo $row->id == 7 ? "selected" : NULL ?>>Religioso</option>
          <option value="8" <?php echo $row->id == 8 ? "selected" : NULL ?>>Didático</option>
          <option value="9" <?php echo $row->id == 9 ? "selected" : NULL ?>>Suspense</option>
        </select>

        <input type="submit" value="Atualizar">
      </form>
    </div>

  <?php } ?>

  <hr />

  <a href="index.php" id="home-btn">Voltar</a>

</body>

</html>