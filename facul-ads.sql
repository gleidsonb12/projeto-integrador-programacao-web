-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 29/10/2020 às 18:17
-- Versão do servidor: 8.0.22
-- Versão do PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `facul-ads`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `acervo`
--

CREATE TABLE `acervo` (
  `id` int NOT NULL,
  `idEditora` int UNSIGNED DEFAULT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `autor` varchar(60) DEFAULT NULL,
  `ano` int DEFAULT NULL,
  `preco` double DEFAULT NULL,
  `quantidade` int DEFAULT NULL,
  `tipo` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Despejando dados para a tabela `acervo`
--

INSERT INTO `acervo` (`id`, `idEditora`, `titulo`, `autor`, `ano`, `preco`, `quantidade`, `tipo`) VALUES
(5, 1, 'Os mais procurados da América', 'Marcos do Val', 2020, 25.99, 200, 2),
(6, 1, 'Quando o sol bater na janela do seu quarto', 'Renato Russo', 2011, 100.99, 100, 4),
(7, 2, 'A grande esperança', 'Ellen White', 1991, 19.87, 1000, 7),
(8, 1, 'Cadê o queijo que estava aqui', 'Rubens do Valle', 1990, 99.99, 120, 9),
(9, 3, 'A arte da guerra', 'Shun Tsu', 1970, 29.99, 137, 6),
(10, 2, 'Biblia Sagrada para Crianças', 'Vários', 2017, 299.99, 2498, 7);

-- --------------------------------------------------------

--
-- Estrutura para tabela `editora`
--

CREATE TABLE `editora` (
  `id` int UNSIGNED NOT NULL,
  `nome` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Despejando dados para a tabela `editora`
--

INSERT INTO `editora` (`id`, `nome`) VALUES
(1, 'Editora Brasil'),
(2, 'Casa Publicadora Brasileira'),
(3, 'Atlas');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `acervo`
--
ALTER TABLE `acervo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idEditora` (`idEditora`);

--
-- Índices de tabela `editora`
--
ALTER TABLE `editora`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `acervo`
--
ALTER TABLE `acervo`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `editora`
--
ALTER TABLE `editora`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `acervo`
--
ALTER TABLE `acervo`
  ADD CONSTRAINT `acervo_ibfk_1` FOREIGN KEY (`idEditora`) REFERENCES `editora` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
